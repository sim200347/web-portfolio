# pull official base image
FROM python:3.9.5

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

RUN apt-get update \
    && apt-get install -y software-properties-common \
    && sed -i "/^# deb.*universe/ s/^# //" /etc/apt/sources.list \
    && apt-get install -y libpq-dev python3-pip gcc python3-dev musl-dev nano \
    && apt-get -y install postgresql ffmpeg libsm6 libxext6  -y\
    && pip3 install --upgrade pip \
    && pip3 install psycopg2

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY . .

RUN python manage.py collectstatic --noinput

RUN adduser ender
USER ender

CMD gunicorn web_portfolio.wsgi:application --bind 0.0.0.0:$PORT
